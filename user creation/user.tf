provider "aws" {
  region = "ap-south-1"
}

resource "aws_iam_user" "user3" {
  name = var.user_name
}

resource "aws_iam_user_policy" "bucket_policy" {
  name   = var.bucket_name
  user   = aws_iam_user.user3.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::var.bucket_name",
        "arn:aws:s3:::var.bucket_name/*"
      ]
    }
  ]
}
EOF
}
